//
//  ViewController.swift
//  iPool V2
//
//  Created by Romuald HAPPE on 20/08/2019.
//  Copyright © 2019 Romuald HAPPE. All rights reserved.
//

import UIKit
import Alamofire

struct JSLogin: Codable {
    let erreur: Int
    let id: Int
    let nom: String
}

struct infoPISCINE: Codable {
    let erreur: Int?
    let tempEau: Int?
    let tempAir: Int?
    let jour: Int?
    let mois: Int?
    let annee: Int?
    let heure: Int?
    let minute: Int?
}

struct tempPISCINE: Codable {
    let erreur: Int
    let tempArtPompe : Int
}

class ViewController: UIViewController {

    var loginPiscine=[JSLogin]()
    var infoPiscine = [infoPISCINE]()
    var temperaturePiscine = [tempPISCINE]()
    
    let API_URL_LOGIN = "http://www.happe.eu.com/login1.php"
    let API_URL_INFO_PISCINE = "http://www.happe.eu.com/infoPiscine.php"
    let API_URL_TEMP_PISCINE = "http://www.happe.eu.com/tempPiscine.php"
    
    var idMembre = "0"

    @IBOutlet weak var loginText: UITextField!
    @IBOutlet weak var mdpText: UITextField!
    @IBOutlet weak var connection: UILabel!
    @IBOutlet weak var tempPiscine: UILabel!
    @IBOutlet weak var tempBrute: UILabel!
    @IBOutlet weak var tempAir: UILabel!
    @IBOutlet weak var nomConnection: UILabel!
    
 
    @IBAction func requestInfoPiscine() {
    
        if connection.text == "Connecté" {
            // Appel requete serveur pour infoPiscine
            let parametersInfoPiscine: Parameters = [
                "id": "\(idMembre)"
            ]
            print(parametersInfoPiscine)
            Alamofire.request(API_URL_INFO_PISCINE, method: .post, parameters: parametersInfoPiscine).responseJSON { response in
                let json = response.data
                do {
                    print("Retour requete : ",response.result.value!)
                    let decoder = JSONDecoder()
                    self.infoPiscine = try decoder.decode([infoPISCINE].self, from:json!)
                    for infoPiscine in self.infoPiscine {
                        self.tempBrute.text = infoPiscine.tempEau?.description
                        self.tempAir.text = infoPiscine.tempAir?.description
                    }
                } catch let err {
                    print(err)
                    print("Fin erreur")
                }
            }
            // Appel requete serveur pour tempPiscine
            let parametersTempPiscine: Parameters = [
                "prop": "\(idMembre)"
            ]
            print(parametersTempPiscine)
            Alamofire.request(API_URL_TEMP_PISCINE, method: .post, parameters: parametersTempPiscine).responseJSON { response in
                let json = response.data
                do {
                    print("Retour requete : ",response.result.value!)
                    let decoder = JSONDecoder()
                    self.temperaturePiscine = try decoder.decode([tempPISCINE].self, from:json!)
                    for temperaturePiscine in self.temperaturePiscine {
                        self.tempPiscine.text = temperaturePiscine.tempArtPompe.description
                    }
                } catch let err {
                    print(err)
                    print("Fin erreur")
                }
            }

        } else{
            let alert = UIAlertController(title: "Erreur", message: "Vous n'êtes pas connecté", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
    }
    }
    
    @IBAction func seConnecter(_ sender: UIButton) {
        
        let username:String = loginText.text!
        let password:String = mdpText.text!
        
        view.endEditing(true)
        
        if (username == "") || (password == "") {
            let alert = UIAlertController(title: "Erreur", message: "Entrez vos identifiants", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                NSLog("The \"OK\" alert occured.")
            }))
            self.present(alert, animated: true, completion: nil)
            
        }
        else{
 
        // Appel requete serveur
            let parameters: Parameters = [
                "pseudo": username,
                "mdp": password
                ]
            
            Alamofire.request(API_URL_LOGIN, method: .post, parameters: parameters).responseJSON { response in
                let json = response.data
                
                do {
                    print(response.result.value!)
                    let decoder = JSONDecoder()
                    self.loginPiscine = try decoder.decode([JSLogin].self, from:json!)
                    for loginPiscine in self.loginPiscine {
                        print(loginPiscine.nom)
                        print(loginPiscine.id)
                        self.connection.text = "Connecté"
                        self.nomConnection.text = loginPiscine.nom
                        self.mdpText.text = ""
                        self.loginText.text = ""
                        self.idMembre = "\(loginPiscine.id)"
                    }
                } catch let err {
                    print(err)
                    print("Fin erreur.")
                }
            }
        }

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        connection.text = "Non Connecté"
        nomConnection.text = ""
        tempAir.text = "NC"
        tempBrute.text = "NC"
        tempPiscine.text = "NC"
        }
    
 }
